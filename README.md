# Easy-Scalable-Procedurally-Generated-Game

# Procedurally Generated 2D Game Prototype Project

The development of 2D games is an area that has grown considerably in recent years, with games like Terraria and Stardew Valley becoming true sales successes. One of the technical challenges faced by developers is the creation of procedural environments that are capable of generating dynamic and interesting worlds for players to explore.

In this context, memory overload is a problem that can arise when trying to create very large and complex worlds, especially when the environment uses many textures and models. It was thinking about this challenge that I devised an object polling strategy to solve the overload problem within memory.

The object polling strategy is a technique used to minimize the memory load that an application needs to support. Instead of dynamically allocating and deallocating objects, the game maintains a pool of pre-allocated objects in memory that are reused as needed.

In the case of the game in question, the object pool is used to manage the textures used in the environment. When the player moves through the game world, the system uses the object polling strategy to load and unload textures according to the player's position. This way, it's possible to create large and complex worlds without overloading the system's memory.

## How to Use

- Main scene location: Assets/Scenes/Proc.Gen.Scene
- Environment generator Assets/Resources/Prefabs - Its possible to define a width to each "Biome"
- Creating new blocks at: Assets/Resources/Scriptables/Blocks

ProceduralEnviromentGenerator
[SerializeField]private float slowUpdateClock = .25f; -> Define when do we should check for player's position

# Projeto de protótipo de jogo procedurally generated 2D

O desenvolvimento de jogos em 2D é uma área que tem crescido bastante nos últimos anos, com jogos como Terraria e Stardew Valley se tornando verdadeiros sucessos de vendas. Um dos desafios técnicos enfrentados pelos desenvolvedores é a criação de ambientes procedurais que sejam capazes de gerar mundos dinâmicos e interessantes para os jogadores explorarem.

Nesse contexto, a sobrecarga de memória é um problema que pode surgir quando se busca criar mundos muito grandes e complexos, especialmente em ambientes que utilizam muitas texturas e modelos. Foi pensando nesse desafio que bolei uma estratégia de object polling para resolver o problema de sobrecarga de memória.

A estratégia de object polling é uma técnica utilizada para minimizar a carga de memória que uma aplicação precisa suportar. Ao invés de alocar e desalocar objetos dinamicamente, o jogo mantém uma pool de objetos pré-alocados em memória que são reutilizados conforme necessário.

No caso do jogo em questão, a pool de objetos é utilizada para gerenciar as texturas utilizadas no ambiente. Quando o jogador se move pelo mundo do jogo, o sistema utiliza a estratégia de object polling para carregar e descarregar as texturas de acordo com a posição do jogador. Dessa forma, é possível criar mundos grandes e complexos sem sobrecarregar a memória do sistema.

## Como usar
