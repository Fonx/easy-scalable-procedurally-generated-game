using UnityEngine;

[CreateAssetMenu(fileName = "New Block", menuName = "Block")]
public class Block : ScriptableObject
{
    public int blockID;
    public string blockName;
    public GameObject top;
    public GameObject Botton;
    private bool isTop = true;
    public bool isSolid;
    private int blockHealth;
    public float scale = 2.5f;
    public bool canFly;
    
    //public bool hasBlockBelow;
    public BlockType type;

    public bool IsTop { get => isTop; set => isTop = value; }

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}

public enum BlockType {Mud, Rock }