using System.Collections.Generic;
using UnityEngine;

public class ItemCombiner : MonoBehaviour
{
    // Classe que representa um item no jogo
    public class Item
    {
        public string name;

        public Item(string name)
        {
            this.name = name;
        }
    }

    // Dicion�rio que mapeia listas de itens combinados para itens �nicos
    Dictionary<List<Item>, Item> combinations = new Dictionary<List<Item>, Item>();

    // M�todo para inicializar o dicion�rio com as combina��es poss�veis
    void InitializeCombinations()
    {
        Item item1 = new Item("Item 1");
        Item item2 = new Item("Item 2");
        Item item3 = new Item("Item 3");
        Item item4 = new Item("Item 4");

        // Adiciona algumas combina��es poss�veis ao dicion�rio
        combinations.Add(new List<Item> { item1, item2 }, new Item("Item 5"));
        combinations.Add(new List<Item> { item1, item3 }, new Item("Item 6"));
        combinations.Add(new List<Item> { item2, item4 }, new Item("Item 7"));
    }

    // M�todo para combinar itens e retornar um item �nico, se poss�vel
    public Item CombineItems(List<Item> items)
    {
        // Procura por uma chave no dicion�rio que corresponda aos itens combinados
        foreach (var kvp in combinations)
        {
            if (ListsEqual(kvp.Key, items))
            {
                // Retorna o valor correspondente se uma chave for encontrada
                return kvp.Value;
            }
        }

        // Retorna null se nenhuma combina��o for encontrada
        return null;
    }

    // M�todo auxiliar para comparar duas listas de itens
    bool ListsEqual(List<Item> a, List<Item> b)
    {
        if (a == null || b == null)
        {
            return a == b;
        }

        if (a.Count != b.Count)
        {
            return false;
        }

        for (int i = 0; i < a.Count; ++i)
        {
            if (a[i].name != b[i].name)
            {
                return false;
            }
        }

        return true;
    }

    void Start()
    {
        // Inicializa o dicion�rio com as combina��es poss�veis
        InitializeCombinations();

        // Exemplo de combina��o de itens
        Item item1 = new Item("Item 1");
        Item item2 = new Item("Item 2");
        List<Item> itemsToCombine = new List<Item> { item1, item2 };
        Item result = CombineItems(itemsToCombine);
        if (result != null)
        {
            Debug.Log("Itens combinados para formar " + result.name);
        }
        else
        {
            Debug.Log("Nenhuma combina��o encontrada");
        }
    }
}