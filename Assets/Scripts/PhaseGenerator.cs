using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Resolve o problema de ter muitos gameObjects na fase com pooling.
/// </summary>
public class PhaseGenerator : MonoBehaviour
{
    public static PhaseGenerator _instance;
    private List<Block> blocks = new List<Block>();

    private List<GameObject>[] pooledBlocks;
    private List<GameObject>[] pooledBlocksOnTop;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        GetBlocksReferences();

        pooledBlocks = new List<GameObject>[blocks.Count];
        for (int i = 0; i < pooledBlocks.Length; i++)
        {
            pooledBlocks[i] = new List<GameObject>();
        }

        pooledBlocksOnTop = new List<GameObject>[blocks.Count];
        for (int i = 0; i < pooledBlocksOnTop.Length; i++)
        {
            pooledBlocksOnTop[i] = new List<GameObject>();
        }
    }

    private void Start()
    {
        GenerateSomePhases(10);
    }

    private void GenerateSomePhases(int width) 
    {
        float offset = 0;
        Vector2 Center = transform.position;
        GameObject[] roomList = Resources.LoadAll<GameObject>("Prefabs/");

        for (int i=0;i< width;i++) {
            int chance = Random.Range(0, roomList.Length);
            GameObject phase = Instantiate(roomList[chance], Center, Quaternion.identity);
            offset = phase.GetComponent<ProceduralEnviromentGenerator>().Width;
            Center += Vector2.right * offset;
        }
    }

    private GameObject PullOrCreate(Block entity, Transform transform, List<GameObject>[] toPull)
    {
        foreach (GameObject block in toPull[entity.blockID])
        {
            if (!block.activeInHierarchy)
            {
                block.transform.SetParent(transform);
                return block;
            }
        }
        GameObject newBlock = Instantiate(entity.IsTop ? entity.top : entity.Botton, transform);
        
        toPull[entity.blockID].Add(newBlock);
        return newBlock;
    }

    public GameObject GetPooledBlock(Block entity, Transform transform)
    {
        if (entity.IsTop)
        {
            return PullOrCreate(entity, transform, pooledBlocks);
        }
        else
        {
            return PullOrCreate(entity, transform, pooledBlocksOnTop);
        }
    }

    private void GetBlocksReferences()
    {
        Block[] scriptables = Resources.LoadAll<Block>("Scriptables/Blocks/");

        for (int i = 0; i < scriptables.Length; i++)
        {
            blocks.Add(scriptables[i]);
        }
        blocks.Sort((t1, t2) => (t1.blockID > t2.blockID) ? 1 : -1);
    }
}
