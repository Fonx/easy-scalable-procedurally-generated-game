using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public static MoveCamera main;

    private void Awake()
    {
        if (main == null)
            main = this;
    }

    void Update()
    {
        Application.targetFrameRate = 60;
        float movimentoHorizontal = Input.GetAxis("Horizontal");
        float movimentoVertical = Input.GetAxis("Vertical");

        transform.position += new Vector3(movimentoHorizontal, movimentoVertical, 0f) / 4;
    }
}