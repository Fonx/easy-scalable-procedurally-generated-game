using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralEnviromentGenerator : MonoBehaviour
{
    private PhaseGenerator generator;
    private Transform content;
    //colocar pooling pra fora e buscar referencias de um controlador.
    [SerializeField] private List<Block> blocks = new List<Block>();
    [SerializeField] private int width = 10;
    [SerializeField] private int height = 10;
    [SerializeField] private float scale = 1f;
    [SerializeField] private int offset = 0;

    [SerializeField]private float slowUpdateClock = .25f;

    int minX = -16;
    int minY = -10;

    private Block[,] grid;

    private float holeProbability = 0.05f; // probabilidade de uma coluna ter uma fissura
    private int maxHoleHeight = 3; // altura m�xima da fissura em cada coluna

    private Transform targetPlayer;

    private float sightIn = 50f;
    private float sightOut = 100f;
    private PerlinNoise noise;

    private List<Block> allocatedtoDebug = new List<Block>();
    public int Width { get => width; set => width = value; }

    private Coroutine currentCreateRotine;
    private void Awake()
    {
        content = transform.GetChild(0);
        targetPlayer = MoveCamera.main.transform;
        generator = PhaseGenerator._instance;
        noise = new PerlinNoise(Random.Range(1000000, 10000000));
        FillGrid();
    }
    private void Start()
    {
        StartCoroutine(TrackPlayer());
    }

    IEnumerator TrackPlayer() {
        WaitForSeconds w8 = new WaitForSeconds(slowUpdateClock);
        while (true)
        {
            if ((transform.position - targetPlayer.position).magnitude < sightIn && !content.gameObject.activeInHierarchy) {
                CreateBlocks();
            }
            else if ((transform.position - targetPlayer.position).magnitude > sightOut && content.gameObject.activeInHierarchy)
            {
                DeleteBlocks();
            }
            yield return w8;
        }
    }

    private void GetBlocksReferences() {

        Block[] scriptables = Resources.LoadAll<Block>("Blocks/");

        for (int i = 0; i < scriptables.Length; i++)
        {
            blocks.Add(scriptables[i]);
        }
        blocks.Sort((t1, t2) => (t1.blockID > t2.blockID) ? 1 : -1);
    }

    private void GenerateLevel()
    {
        FillGrid();
        CreateBlocks();
    }

    private void FillGridLargeHoles()
    {
        grid = new Block[width, height];

        for (int x = 0; x < width; x++)
        {
            //int columnHeight = Random.Range(1, maxHeight + 1); // define a altura da parede para esta coluna
            int columnHeight = Random.Range(1, 25 + 1);
            int holeHeight = Random.Range(1, maxHoleHeight + 1); // define a altura da fissura para esta coluna

            for (int y = 0; y < height; y++)
            {
                if (y >= columnHeight) // checa se estamos acima da altura da parede
                {
                    grid[x, y] = null; // define o bloco como vazio
                }
                else
                {
                    if (y >= columnHeight - holeHeight && Random.value < holeProbability) // checa se estamos na altura da fissura e se ela deve ser criada
                    {
                        grid[x, y] = null; // define o bloco como vazio
                    }
                    else
                    {
                        float noiseValue = Mathf.PerlinNoise((float)x / width * scale + offset, (float)y / height * scale + offset);
                        int blockIndex = Mathf.FloorToInt(noiseValue * blocks.Count);
                        Block selectedBlock = (Block)blocks[blockIndex].Clone();

                        grid[x, y] = selectedBlock;
                    }
                }
            }
        }
    }
    private void FillGrid()
    {
        grid = new Block[width, height];

        for (int x = 0; x < width; x++)
        {
            //int columnHeight = Random.Range(1, maxHeight + 1); // define a altura da parede para esta coluna
            int columnHeight = 2 + noise.getNoise(x - minX, height - minY - 2);
            int holeHeight = Random.Range(1, maxHoleHeight + 1); // define a altura da fissura para esta coluna

            for (int y = 0; y < height; y++)
            {
                if (y >= columnHeight) // checa se estamos acima da altura da parede
                {
                    grid[x, y] = null; // define o bloco como vazio
                }
                else
                {
                    if (y >= columnHeight - holeHeight && Random.value < holeProbability) // checa se estamos na altura da fissura e se ela deve ser criada
                    {
                        grid[x, y] = null; // define o bloco como vazio
                    }
                    else
                    {
                        float noiseValue = Mathf.PerlinNoise((float)x / width * scale + offset, (float)y / height * scale + offset);
                        int blockIndex = Mathf.FloorToInt(noiseValue * blocks.Count);
                        Block selectedBlock = (Block)blocks[blockIndex].Clone();

                        grid[x, y] = selectedBlock;
                    }
                    allocatedtoDebug.Add(grid[x, y]);
                }
            }
        }
    }

    private void CreateBlocks()
    {
        content.gameObject.SetActive(true);
        currentCreateRotine = StartCoroutine(CreateBlocksRotine());

/*        content.gameObject.SetActive(true);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Block selectedBlock = grid[x, y];
                if (selectedBlock != null) {
                    if (y < height -1) // checa se estamos na primeira linha
                    {
                        // verifica se h� um bloco acima deste
                        Block blockBelow = grid[x, y + 1];
                        if (blockBelow != null)
                        {
                            selectedBlock.IsTop = false;
                        }
                    }

                    if (selectedBlock.canFly || selectedBlock.IsTop) // cria o bloco se houver um abaixo
                    {
                        GameObject blockObject = GetPooledBlock(selectedBlock);
                        blockObject.transform.position = transform.position + new Vector3(x, y,0);
                        blockObject.transform.localScale = Vector3.one * selectedBlock.scale;
                        blockObject.SetActive(true);
                    }
                }
            }
        }*/
    }

    IEnumerator CreateBlocksRotine() {
        WaitForEndOfFrame w8 = new WaitForEndOfFrame();
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Block selectedBlock = grid[x, y];
                if (selectedBlock != null)
                {
                    if (y < height - 1) // checa se estamos na primeira linha
                    {
                        // verifica se h� um bloco acima deste
                        Block blockBelow = grid[x, y + 1];
                        if (blockBelow != null)
                        {
                            selectedBlock.IsTop = false;
                        }
                    }

                    if (selectedBlock.canFly || selectedBlock.IsTop) // cria o bloco se houver um abaixo
                    {
                        GameObject blockObject = GetPooledBlock(selectedBlock);
                        blockObject.transform.position = transform.position + new Vector3(x, y, 0);
                        blockObject.transform.localScale = Vector3.one * selectedBlock.scale;
                        blockObject.SetActive(true);
                    }
                }
            }
            yield return w8;
        }
    }

    private void DeleteBlocks()
    {
        content.gameObject.SetActive(false);
        StopCoroutine(currentCreateRotine);
    }

    private GameObject GetPooledBlock(Block entity)
    {
        return generator.GetPooledBlock(entity, content);
    }
}